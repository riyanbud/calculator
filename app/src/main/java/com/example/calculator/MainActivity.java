package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

        Button bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, bt0, btClear, btHasil, btEnter, btJumlah, btKurang, btBagi, btKali, btkoma;
        TextView angka;
        String bilangan;
        double angka1, angka2, jumlah;
        boolean hasilJumlah, hasilKurang, hasilKali, hasilBagi;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);

                angka = findViewById(R.id.angka);
                bt1 = findViewById(R.id.bt1);
                bt2 = findViewById(R.id.bt2);
                bt3 = findViewById(R.id.bt3);
                bt4 = findViewById(R.id.bt4);
                bt5 = findViewById(R.id.bt5);
                bt6 = findViewById(R.id.bt6);
                bt7 = findViewById(R.id.bt7);
                bt8 = findViewById(R.id.bt8);
                bt9 = findViewById(R.id.bt9);
                bt0 = findViewById(R.id.bt0);

                btClear = findViewById(R.id.btClear);
                btEnter = findViewById(R.id.btClear);
                btHasil = findViewById(R.id.btHasil);
                btJumlah = findViewById(R.id.btJumlah);
                btKurang = findViewById(R.id.btKurang);
                btKali = findViewById(R.id.btKali);
                btBagi = findViewById(R.id.btBagi);

                bt0.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "1");
                        }
                });
                bt1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "1");
                        }
                });
                bt2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "2");
                        }
                });
                bt3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "3");
                        }
                });
                bt4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "4");
                        }
                });
                bt5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "5");
                        }
                });
                bt6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "6");
                        }
                });
                bt7.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "7");
                        }
                });
                bt8.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "8");
                        }
                });
                bt9.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText(angka.getText() + "9");
                        }
                });
                btJumlah.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                if (angka == null) {
                                        angka.setText("");
                                } else {
                                        angka1 = Float.parseFloat(angka.getText() + "");
                                        hasilJumlah = true;
                                        angka.setText(null);
                                }
                        }
                });
                btKurang.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka1 = Float.parseFloat(angka.getText() + "");
                                hasilKurang = true;
                                angka.setText(null);
                        }
                });
                btKali.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka1 = Float.parseFloat(angka.getText() + "");
                                hasilKali = true;
                                angka.setText(null);
                        }
                });
                btBagi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka1 = Float.parseFloat(angka.getText() + "");
                                hasilBagi = true;
                                angka.setText(null);
                        }
                });
                btHasil.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka2 = Float.parseFloat(angka.getText() + "");

                                if (hasilJumlah == true) {
                                        angka.setText(angka1 + angka2 + "");
                                        hasilJumlah = false;
                                }
                                if (hasilKurang == true) {
                                        angka.setText(angka1 - angka2 + "");
                                        hasilKurang = false;
                                }
                                if (hasilKali == true) {
                                        angka.setText(angka1 * angka2 + "");
                                        hasilKali = false;
                                }
                                if (hasilBagi == true) {
                                        angka.setText(angka1 / angka2 + "");
                                        hasilBagi = false;
                                }
                        }
                });

                btClear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                angka.setText("");
                        }
                });
        }
}